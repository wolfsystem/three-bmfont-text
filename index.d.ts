import { BufferGeometry } from "three"

export interface Glyph {
  index   : number           // the index of this glyph into the string
  data    : object           // the BMFont "char" object for this glyph
  position: [number, number] // the baseline position to render this glyph
  line    : number           // the line index this glyph appears in
}

export interface Layout {
  font         : object // the BMFont definition which holds chars, kernings, etc
  text         : string // the text to layout. Newline characters (\n) will cause line breaks
  width        : number // the desired width of the text box, causes word-wrapping and clipping in "pre" mode. Leave as undefined to remove word-wrapping (default behaviour)
  height       : number
  mode         : "pre" | "nowrap" // a mode for word-wrapper; can be 'pre' (maintain spacing), or 'nowrap' (collapse whitespace but only break on newline characters), otherwise assumes normal word-wrap behaviour (collapse whitespace, break at width or newlines)
  align        : "left" | "center" | "right"  // default: left
  letterSpacing: number // the letter spacing in pixels (default: 0)
  lineHeight   : number // the line height in pixels (default to font.common.lineHeight)
  tabSize      : number // the number of spaces to use in a single tab (default 4)
  start        : number // the starting index into the text to layout (default 0)
  end          : number // the ending index (exclusive) into the text to layout (default text.length)
  glyphs       : Glpyh[]
  baseline     : number // The baseline metric: measures top of text layout to the baseline of the first line.
  xHeight      : number // The x-height metric; the height of a lowercase character. Uses the first available height of the common lowercase Latin "x-chars", such as 'x', 'u', 'v', 'w', 'z'.
  descender    : number // The metric from baseline to the bottom of the descenders (like the bottom of a lowercase "g").
  ascender     : number // The metric for ascenders; typically from the top of x-height to the top of the glyph height.
  capHeight    : number // The cap height metric; the height of a flat uppercase letter like 'H' or 'I'. Uses the frist available height of common uppercase Latin flat capitals, such as 'H', 'I', 'E', 'T', 'K'.
  lineHeight   : number // The line height; the height from one baseline to the next. This is what was passed as opt.lineHeight, or defaults to the font.common.lineHeight.
}

export class TextGeometry extends BufferGeometry {
  public update(opt: string | LayoutOpt): void
  public layout: Layout
  public visibleGlyphs: Glpyh[]
}

export interface LayoutOpt {
  text          : string
  font?         : object
  width?        : number
  mode?         : "pre" | "nowrap"
  align?        : "left" | "center" | "right"
  letterSpacing?: number
  lineHeight?   : number
  tabSize?      : number
  start?        : number
  end?          : number
  layoutHint?   : Layout
}

export default function createTextGeometry(opt: string | LayoutOpt): TextGeometry
