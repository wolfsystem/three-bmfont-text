declare function createMSDFShader(opt:
  { opacity  : number
  , alphaTest: number
  //, precision: string
  , side     : THREE.Side
  , color    : THREE.Color | string | number
  , map      : THREE.Texture
  , zOffset  : number
  } | THREE.ShaderMaterialParameters ) : THREE.ShaderMaterialParameters

export = createMSDFShader
