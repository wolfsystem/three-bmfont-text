var assign = require('object-assign')
var THREE  = require('three')

module.exports = function createMSDFShader (opt) {
  opt = opt || {}
  var opacity   = typeof opt.opacity   === 'number' ? opt.opacity : 1
  var alphaTest = typeof opt.alphaTest === 'number' ? opt.alphaTest : 0.0001
  var precision = opt.precision || 'highp'
  var color     = opt.color
  var map       = opt.map
  var negate    = typeof opt.negate === 'boolean' ? opt.negate : true
  var zOffset   = opt.zOffset
  var clipping = typeof opt.clipping === 'boolean' ? opt.clipping : false

  // remove to satisfy r73
  delete opt.map
  delete opt.color
  delete opt.precision
  delete opt.opacity
  delete opt.negate
  delete opt.zOffset

  // https://github.com/mrdoob/three.js/tree/r124/src/renderers/shaders/ShaderChunk
  return assign({
    uniforms: {
      opacity: { type: 'f', value: opacity },
      map    : { type: 't', value: map || new THREE.Texture() },
      color  : { type: 'c', value: new THREE.Color(color) },
      zOffset: { type: 'f', value: zOffset },
    },
    vertexShader: `#version 300 es
    uniform mat4  projectionMatrix;
    uniform mat4  modelViewMatrix;
    uniform float zOffset;
    in      vec2  uv;
    in      vec3  position;
    out     vec2  vUv;
    ${clipping ? `#if NUM_CLIPPING_PLANES > 0
      out vec3 vClipPosition;
    #endif` : ''}
    
    void main() {
      vUv = uv;
      vec3 transformed = position + vec3(0, 0, zOffset);
      #include <project_vertex>
      ${clipping ? `#include <clipping_planes_vertex>` : ''}
    }`,
    fragmentShader: `#version 300 es
    #ifdef     GL_OES_standard_derivatives
    #extension GL_OES_standard_derivatives : enable
    #endif
    precision ${precision} float;
    uniform float     opacity;
    uniform vec3      color;
    uniform sampler2D map;
    in      vec2      vUv;
    out     vec4      fragColor;
    ${clipping ? `#if NUM_CLIPPING_PLANES > 0
      in      vec3 vClipPosition;
      uniform vec4 clippingPlanes[ NUM_CLIPPING_PLANES ];
    #endif` : ''}  

    float median(float r, float g, float b) {
      return max(min(r, g), min(max(r, g), b));
    }

    void main() {
      ${clipping ? `#include <clipping_planes_fragment>` : ''}
      vec3 texsample = ${negate ? '1.0 -' : ''} texture(map, vUv).rgb;
      float sigDist = median(texsample.r, texsample.g, texsample.b) - 0.5;
      float alpha = clamp(sigDist/fwidth(sigDist) + 0.5, 0.0, 1.0);
      fragColor = vec4(color.xyz, alpha * opacity);
      ${alphaTest === 0
        ? ''
        : '  if (fragColor.a < ' + alphaTest + ') discard;'
      }
    }`
  }, opt)
}
