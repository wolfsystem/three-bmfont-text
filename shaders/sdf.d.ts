declare function createSDFShader(opt:
  { opacity  : number
  , alphaTest: number
  //, precision: string
  , side     : THREE.Side
  , color    : THREE.Color | string | number
  , map      : THREE.Texture
  } | THREE.ShaderMaterialParameters ) : THREE.ShaderMaterialParameters

export = createSDFShader
