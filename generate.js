const generateBMFont = require('msdf-bmfont')
const fs             = require('fs')

const fileRob = "Roboto-Regular"
const fileAws = "fontawesome-webfont"
const fileFAR = "fa-regular-400"
const fileFAS = "fa-solid-900"

// CONFIG
const file = fileRob
const opt =
    { "charset"       : "abcdefghijklmnopqrstuvwxyz"         +
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"         +
                        "äöüÄÖÜß"                            +
                        "0123456789"                         +
                        ' !"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~' +
                        "€²°"                                
    , "fontSize"      :  60
    , "textureWidth"  : 512
    , "textureHeight" : 512
    , "texturePadding":   1
    , "transparent"   : false
    , "fieldType"     : "msdf"
    , "distanceRange" :   3
    }
//

generateBMFont(file + ".ttf", opt, (error, textures, font) => {
  if (error) throw error
  if (textures.length > 1 ) {
      textures.forEach((sheet, index) => {
        font.pages.push("sheet" + index + ".png")
        fs.writeFile("sheet" + index + ".png", sheet, (err) => {if (err) throw err})
      })
  } else {
    fs.writeFile(file + ".png", textures[0], (err) => {if (err) throw err})
  }
  fs.writeFile(file + ".json", JSON.stringify(font), (err) => {if (err) throw err})
})
