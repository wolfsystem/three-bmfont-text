#!/bin/bash
# install via: npm install -g msdf-bmfont-xml
echo "Generate font"

FILE_ROB=Roboto-Regular.ttf
FILE_AWS=fontawesome-webfont.ttf
FILE_FAR=fa-regular-400.ttf
FILE_FAS=fa-solid-900.ttf
FILE_FAB=fa-brands-400.ttf
FILE_DIN=DIN_CB.ttf
FILE_FIR=FiraSansMedium.woff
FILE_HYH=HYHeiLiZhiTiJ.ttf
FILE_SRC=SourceSansPro-Regular.otf
FONT_FILE=$FILE_ROB
msdf-bmfont --output-type json --font-size 36 --charset-file charset.txt --texture-size 256,256 --field-type msdf ${FONT_FILE} --vector
convert "${FONT_FILE%%.*}.png" -channel RGB -negate "${FONT_FILE%%.*}.png"

echo "...done"
echo "Please press the Any-key!"
read -rsn1
